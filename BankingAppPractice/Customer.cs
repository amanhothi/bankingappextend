﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingAppPractice
{
    class Customer
    {
        string name;
        int id;
        double cheqAcc;
        double savingAcc;
        public Customer(string n, int id1, double chequing, double saving)
        {
            this.name = n;
            this.id = id1;
            this.cheqAcc = chequing;
            this.savingAcc = saving;

        }
        public void showInfo()
        {
            Console.WriteLine("Name: " + this.name + "Account number: " + this.id + "cheqing" + this.cheqAcc + "saving" + this.savingAcc);
        }

        public string Name
        {
            get
            {
                return this.name;
            }

        }
        public int Id
        {
            get
            {
                return this.id;
            }

        }
        public double CheqAcc
        {
            get
            {
                return this.cheqAcc;
            }

        }
        public double SavingAcc
        {
            get
            {
                return this.savingAcc;
            }

        }
    }
}
