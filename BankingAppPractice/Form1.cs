﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankingAppPractice
{
    public partial class Form1 : Form
    {
        List<Customer> custList = new List<Customer>();
        string name = "";
        int id;
        double cheqAcc = 0;
        double savingAcc=0;
       
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
             name = textBox1.Text;
             id = Convert.ToInt32(textBox2.Text);
             cheqAcc = Convert.ToDouble(textBox3.Text);
             savingAcc = Convert.ToDouble(textBox4.Text);

            Customer customer = new Customer(name, id, cheqAcc, savingAcc);
            custList.Add(customer);
            Console.WriteLine("Added customer");
            customer.showInfo();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int custId = Convert.ToInt32(textBox5.Text);
            for (int i=0;i<custList.Count; i++)
            {
               // int customerId = custList[i].Id;
                if (custId == custList[i].Id)
                {
                    Console.WriteLine("Welcome" + custList[i].Name);
                    label6.Text = "Welcome" + custList[i].Name;

                    // "break" exits the loop early
                    break;
                }
                else
                {
                    label6.Text = custId +" Cutomer does not exist";
                }
                Console.WriteLine(custList[i].Name);
                Console.WriteLine(custList[i].Id);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int custId = Convert.ToInt32(textBox5.Text);
            if (radioButton1.Checked == true)
            {
                for (int i = 0; i < custList.Count; i++)
                {
                    // int customerId = custList[i].Id;
                    if (custId == custList[i].Id)
                    {
                        Console.WriteLine(custList[i].CheqAcc);
                        cheqAcc = custList[i].CheqAcc;
                        label7.Text = "Your balance is" +cheqAcc;
                        
                        // "break" exits the loop early
                        break;
                    }
                    
                }
                
            }
            else if (radioButton2.Checked == true)
            {
                for (int i = 0; i < custList.Count; i++)
                {
                    // int customerId = custList[i].Id;
                    if (custId == custList[i].Id)
                    {
                        Console.WriteLine(custList[i].SavingAcc);
                        savingAcc = custList[i].SavingAcc;
                        label7.Text = "Your balance is" + savingAcc;

                        // "break" exits the loop early
                        break;
                    }
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            double withdraw = Convert.ToDouble(textBox6.Text);
            int custId = Convert.ToInt32(textBox5.Text);
            if (radioButton1.Checked == true)
            {
                for (int i = 0; i < custList.Count; i++)
                {
                    // int customerId = custList[i].Id;
                    //double newCheq = custList[i].CheqAcc;
                    if (custId == custList[i].Id)
                    {
                        Console.WriteLine(custList[i].CheqAcc);
                        //cheqAcc = custList[i].CheqAcc;
                        cheqAcc =  cheqAcc- withdraw;
                        label7.Text = "Your Balance is :" + cheqAcc;
                        label10.Text = "Withdraw successful";
                        // "break" exits the loop early
                       // break;
                    }
                }

            }
            else if (radioButton2.Checked == true)
            {
                for (int i = 0; i < custList.Count; i++)
                {
                    // int customerId = custList[i].Id;
                    if (custId == custList[i].Id)
                    {
                        Console.WriteLine(custList[i].SavingAcc);
                        savingAcc = custList[i].SavingAcc - withdraw;
                        label7.Text = "Your Balance is :" + savingAcc;
                        label10.Text = "Withdraw successful";
                        // "break" exits the loop early
                        break;
                    }
                }
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            double deposit = Convert.ToDouble(textBox7.Text);
            int custId = Convert.ToInt32(textBox5.Text);
            if (radioButton1.Checked == true)
            {
                for (int i = 0; i < custList.Count; i++)
                {
                    // int customerId = custList[i].Id;
                    //double newCheq = custList[i].CheqAcc;
                    if (custId == custList[i].Id)
                    {
                        Console.WriteLine(custList[i].CheqAcc);
                        cheqAcc =cheqAcc +deposit;
                        label7.Text = "Your Balance is :" + cheqAcc;
                        label10.Text = "Deposit successful";
                        // "break" exits the loop early
                        break;
                    }
                }

            }
            else if (radioButton2.Checked == true)
            {
                for (int i = 0; i < custList.Count; i++)
                {
                    // int customerId = custList[i].Id;
                    if (custId == custList[i].Id)
                    {
                        Console.WriteLine(custList[i].SavingAcc);
                        savingAcc = custList[i].SavingAcc +deposit;
                        label7.Text = "Your Balance is :" + savingAcc;
                        label10.Text = "Deposit successful";
                        // "break" exits the loop early
                        break;
                    }
                }
            }
        }
    }
}
